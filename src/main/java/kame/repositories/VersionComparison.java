package kame.repositories;

import org.apache.commons.lang3.math.NumberUtils;
import org.jetbrains.annotations.NotNull;

class VersionComparison {
    public static int compare(@NotNull String a, @NotNull String b) {
        var thisParts = a.split("[.]");
        var thatParts = b.split("[.]");
        int length = Math.max(thisParts.length, thatParts.length);
        for(var i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ? NumberUtils.toInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ? NumberUtils.toInt(thatParts[i]) : 0;
            if(thisPart < thatPart) return -1;
            if(thisPart > thatPart) return +1;
        }
        return 0;
    }
}
