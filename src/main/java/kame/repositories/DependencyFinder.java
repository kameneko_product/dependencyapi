package kame.repositories;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

/**
 * GitLab Packagesからパッケージ情報を取得する機能を提供します。
 */
public final class DependencyFinder {

    private static final Gson parser = new GsonBuilder()
            .registerTypeAdapter(Date.class, new GsonAdapters.UTCDateAdapter())
            .registerTypeAdapter(GitlabPackage.class, new GsonAdapters.GitlabPackageAdapter())
            .registerTypeAdapter(GitlabPipeline.class, new GsonAdapters.GitlabPipelineAdapter())
            .create();

    /**
     * パッケージ一覧を取得します。
     * @param url パッケージ一覧を取得するURL
     * @param name 指定の名前でフィルターするパッケージ名を指定します。nullを指定した場合は全てのパッケージを取得します。
     * @return パッケージ一覧
     */
    public static GitlabPackage[] getPackages(@NotNull URL url, @Nullable String name) throws IOException {
        var connection = url.openConnection();
        try (var reader = new InputStreamReader(connection.getInputStream())) {
            return Arrays.stream(parser.fromJson(reader, GitlabPackage[].class))
                    .filter(x -> "maven".equals(x.package_type()))
                    .filter(x -> name == null || name.equals(x.name()))
                    .toArray(GitlabPackage[]::new);
        }
    }

    /**
     * パッケージの最新のバージョンを取得します。
     * @param url パッケージ一覧を取得するURL
     * @param name 最新のバージョンを取得するパッケージ名
     * @return パッケージ一覧の最新のバージョン情報
     */
    public static Optional<PackageVersion> getLatestVersion(@NotNull URL url, @NotNull String name) throws IOException {
        return Arrays.stream(getPackages(url, name))
                .max(Comparator.comparing(GitlabPackage::version, VersionComparison::compare))
                .flatMap(x -> Arrays.stream(x.pipelines())
                        .filter(y -> "success".equals(y.status()))
                        .max(Comparator.comparing(GitlabPipeline::updated_at))
                        .map(y -> new PackageVersion(x, y.updated_at())));
    }

    /**
     * グループのパッケージ一覧を取得するURLを取得します。
     * @param group_id グループID
     * @return Gitlab packages API の URL
     */
    public static String getGroupPackageUrl(int group_id) {
        return "https://gitlab.com/api/v4/groups/%s/packages".formatted(group_id);
    }

    /**
     * プロジェクトのパッケージ一覧を取得するURLを取得します。
     * @param project_id プロジェクトID
     * @return Gitlab packages API の URL
     */
    public static String getPackageUrl(int project_id) {
        return "https://gitlab.com/api/v4/projects/%s/packages".formatted(project_id);
    }

    /**
     * パッケージをダウンロードするURLを取得します。
     * @param project_id Gitlab プロジェクトID
     * @param group_id Maven グループID
     * @param artifact_id Maven アーティファクトID
     * @param version パッケージバージョン
     * @return パッケージダウンロード用のURL
     */
    @Deprecated
    public static String getDownloadUrl(int project_id, String group_id, String artifact_id, String version) {
        var format = "https://gitlab.com/api/v4/projects/%s/packages/maven/%s/%s/%s/%s-%s.jar";
        return format.formatted(project_id, group_id, artifact_id, version, artifact_id, version);
    }
}
