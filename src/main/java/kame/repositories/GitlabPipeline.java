package kame.repositories;

import java.util.Date;

/**
 * Package Registry のパイプラインの情報を定義したレコードです。
 * @param status CI実行結果
 * @param updated_at パイプラインの状態が更新された時間
 */
public record GitlabPipeline(String status, Date updated_at) {
}
