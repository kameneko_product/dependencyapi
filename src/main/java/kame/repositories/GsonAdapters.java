package kame.repositories;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


final class GsonAdapters {
    static class UTCDateAdapter implements JsonDeserializer<Date> {

        private final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);

        public UTCDateAdapter() {
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        @Override
        public Date deserialize(JsonElement json, Type type, JsonDeserializationContext context) {
            try {
                return dateFormat.parse(json.getAsString());
            } catch (ParseException e) {
                throw new JsonParseException(e);
            }
        }
    }

    static class GitlabPackageAdapter implements JsonDeserializer<GitlabPackage> {

        @Override
        public GitlabPackage deserialize(JsonElement json, Type type, JsonDeserializationContext context) {
            var obj = json.getAsJsonObject();
            var project_id = obj.get("project_id").getAsInt();
            var name = obj.get("name").getAsString();
            var version = obj.get("version").getAsString();
            var package_type = obj.get("package_type").getAsString();
            GitlabPipeline[] pipelines = context.deserialize(obj.get("pipelines"), GitlabPipeline[].class);
            return new GitlabPackage(project_id, name, version, package_type, pipelines);
        }
    }

    static class GitlabPipelineAdapter implements JsonDeserializer<GitlabPipeline> {

        @Override
        public GitlabPipeline deserialize(JsonElement json, Type type, JsonDeserializationContext context) {
            var obj = json.getAsJsonObject();
            var status = obj.get("status").getAsString();
            return new GitlabPipeline(status, context.deserialize(obj.get("updated_at"), Date.class));
        }
    }
}
