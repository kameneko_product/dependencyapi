package kame.repositories;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Date;

/**
 * Package Registry に登録されているパッケージのバージョンなどの情報を定義したレコードです。
 * @param project_id GitLabプロジェクトの project id
 * @param name プロジェクトの名前
 * @param version プロジェクトのバージョン
 * @param updated_at パイプラインの最新の更新日時
 */
public record PackageVersion(int project_id, String name, String version, Date updated_at) {
    public PackageVersion(GitlabPackage pack, Date updated_at) {
        this(pack.project_id(), pack.name(), pack.version(), updated_at);
    }

    /**
     * nameフィールドからgroupIdを取得します
     * @return groupIdの文字列、nameが空の場合は空の文字列
     */
    public String groupId() {
        var ids = name.split("/", 2);
        return ids.length > 0 ? ids[0] : "";
    }

    /**
     * nameフィールドからartifactIdを取得します
     * @return artifactIdの文字列、nameが空の場合、または、/で区切られていない場合は空の文字列
     */
    public String artifactId() {
        var ids = name.split("/", 2);
        return ids.length > 1 ? ids[1] : "";
    }

    /**
     * このインスタンスの情報からパッケージのダウンロードURLを取得します。
     * @return Gitlabのパッケージレジストリ上にあるjarファイルのURL
     */
    public String getDownloadUrl() {
        var format = "https://gitlab.com/api/v4/projects/%s/packages/maven/%s/%s/%s/%s-%s.jar";
        return format.formatted(project_id, groupId(), artifactId(), version, artifactId(), version);
    }

    /**
     * パッケージを指定したパスへダウンロードをします。
     * @param path ダウンロードしたファイルを保存するパス
     * @throws IOException URLが不正またはダウンロード時に何らかの例外が発生した場合
     */
    public void download(Path path) throws IOException {
        var url = new URL(getDownloadUrl());
        try (var stream = url.openStream()) {
            Files.createDirectories(path.getParent());
            Files.copy(stream, path, StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
