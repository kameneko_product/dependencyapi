package kame.repositories;

import java.util.Arrays;

/**
 * Package Registry に登録されているパッケージ情報を定義したレコードです。
 * @param project_id プロジェクトID
 * @param name パッケージ名
 * @param version パッケージバージョン
 * @param package_type 種類 (maven)
 * @param pipelines パイプライン一覧
 */
public record GitlabPackage(int project_id, String name, String version, String package_type, GitlabPipeline[] pipelines) {
    /**
     * nameフィールドからgroupIdを取得します
     * @return groupIdの文字列、nameが空の場合は空の文字列
     */
    public String groupId() {
        var ids = name.split("/", 2);
        return ids.length > 0 ? ids[0] : "";
    }

    /**
     * nameフィールドからartifactIdを取得します
     * @return artifactIdの文字列、nameが空の場合、または、/で区切られていない場合は空の文字列
     */
    public String artifactId() {
        var ids = name.split("/", 2);
        return ids.length > 1 ? ids[1] : "";
    }
}
