import kame.repositories.DependencyFinder;
import org.junit.jupiter.api.Test;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;

public class ApiTest {
    private static final String PACKAGE_URL = "https://gitlab.com/api/v4/groups/54021868/packages";
    private static final String PACKAGE_NAME = "kame/DependencyAPI";
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    /**
     * パッケージ一覧の取得
     *
     * @throws Exception テストに失敗した場合
     */
    @Test
    void listPackage() throws Exception {
        var packages = DependencyFinder.getPackages(new URL(PACKAGE_URL), null);
        System.out.println("[Package list test] " + packages.length + " Packages found.");
        for (var pack : packages) {
            System.out.println("- [" + pack.package_type() + "] id:" + pack.project_id() + " " + pack.name() + " " + pack.version());
            for (var pipeline : pack.pipelines()) {
                System.out.println("  pipeline:" + pipeline.status() + " at " + DATE_FORMAT.format(pipeline.updated_at()));
            }
        }
    }

    /**
     * 最新パッケージの取得
     *
     * @throws Exception テストに失敗した場合
     */
    @Test
    void getLatest() throws Exception {
        var latestOption = DependencyFinder.getLatestVersion(new URL(PACKAGE_URL), PACKAGE_NAME);
        if (latestOption.isPresent()) {
            var pack = latestOption.get();
            System.out.println("[Package data] id:" + pack.project_id() + " " + pack.name());
            System.out.println("[Last version] " + pack.version() + " at " + DATE_FORMAT.format(pack.updated_at()));
            var path = Path.of(pack.artifactId() + "-" + pack.version() + ".jar").toAbsolutePath();
            System.out.println("[Package downloading] " + path);
            pack.download(path);
            System.out.println("Download completed. size= " + Files.size(path) + "bytes");
        }
    }
}
